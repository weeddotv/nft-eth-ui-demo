import axios from "axios";
import { AUTH_TOKEN } from "../utils/constants";
import CookieService from "./CookieService";


class ConfigService {
  token = null;
  constructor() {
    this.userAxiosInstance = axios.create({
      baseURL: process.env.NEXT_PUBLIC_USER_SERVICE,
      timeout: 99999999,
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
    });
    // reload auth token
    if (typeof window !== "undefined") {
      this.setToken(CookieService.get(AUTH_TOKEN));
    }
  }

  getToken = () => {
    return this.token;
  };

  setToken = (token) => {
    if (token) {
      CookieService.set(AUTH_TOKEN, token, 365);
      this.userAxiosInstance.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${token}`;
    } else {
      this.userAxiosInstance.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${null}`;

      // delete this.userAxiosInstance.defaults.headers.common.Authorization;
      CookieService.erase(AUTH_TOKEN);
    }
    this.token = token;
  };
}

export default ConfigService;
