import WalletConnectProvider from "@walletconnect/web3-provider";
import QRCodeModal from "@walletconnect/qrcode-modal";
import Web3 from "web3";
import { isAndroid } from "react-device-detect";

import { convertUtf8ToHex } from "../utils/web3Utilities";

var ethUtil = require("ethereumjs-util");
var sigUtil = require("eth-sig-util");

class Web3Service {
  provider = null;
  currentAccount = null;
  chainId = "0x89";
  subscribed = false;
  web3 = null;
  confirmed = false;
  initialize = async () => {
    if (window?.ethereum) {
      this.provider = window?.ethereum;
      this.web3 = new Web3(this.provider);
    } else {
      await this.initializeWalletConnect();
    }
  };

  setCurrentAccount = (currentAccount) => {
    this.currentAccount = currentAccount;
  };

  setChainId = (chainId) => {
    // this.chainId = chainId;
    // comment to support all chainId
  };

  setTransactionHash = (transactionHash) => {
    this.transactionHash = transactionHash;
  };

  handleAccountsChanged = async (accounts) => {
    if (accounts.length === 0) {
      // MetaMask is locked or the user has not connected any accounts
      this.disconnect();
      this.setCurrentAccount(null);
    } else if (accounts[0] !== this.currentAccount) {
      this.setCurrentAccount(accounts[0]);
      // Do any other work!
      this.subscription();
    }
  };

  subscription = async () => {
    if (!this.subscribed) {
      this.provider.on("connect", this.handleConnect);
      this.provider.on("disconnect", this.handleDisconnect);
      this.provider.on("accountsChanged", this.handleAccountsChanged);
      this.provider.on("chainChanged", this.handleChainChanged);
      this.subscribed = true;
    }
  };

  handleChainChanged = (chainId) => {
    if (chainId !== this.chainId) {
      window.location.reload();
    }
  };

  handleDisconnect = (error) => {
    console.log("-----handleDisconnect", error);
    this.disconnect();
  };

  handleError = (error) => {
    console.log("-----handleError", error);
  };
  handleConnect = (connectInfo) => {
    console.log("-----handleConnect", connectInfo);
  };

  connect = () => {
    return new Promise((resolve, reject) => {
      if (!this.connected) {
        this.provider
          .request({ method: "eth_requestAccounts" })
          .then(this.handleAccountsChanged)
          .then(() => {
            resolve();
          })
          .then(() => {})
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve();
      }
    });
  };

  initializeWalletConnect = async () => {
    this.provider = new WalletConnectProvider({
      infuraId: "27e484dcd9e3efcfd25a83a78777cdf1",
      rpc: {
        137: "https://polygon-rpc.com/",
        80001: "https://matic-mumbai.chainstacklabs.com",
      },
      bridge: "https://bridge.walletconnect.org",
      qrcodeModalOptions: {
        mobileLinks: ["metamask"],
        desktopLinks: ["encrypted ink"],
      },
    });

    setTimeout(() => {
      if (isAndroid) {
        const connectButton = document.getElementById(
          "walletconnect-connect-button-Connect"
        );
        connectButton.setAttribute("target", "_self");
      }
    }, 300);
    await this.provider.enable();
    this.web3 = new Web3(this.provider);
  };

  connectWallet = async () => {
    try {
      await this.provider.connector.on("display_uri", (err, payload) => {
        const uri = payload.params[0];
        QRCodeModal.display(uri);
      });
      const accounts = await this.web3.eth.getAccounts();
      this.handleAccountsChanged(accounts);
    } catch (e) {
      console.log("----connectWallet error:", e);
    }
  };

  personalSign = ({ message }) => {
    return new Promise((resolve, reject) => {
      const msg = ethUtil.bufferToHex(new Buffer(message, "utf8"));
      const from = this.currentAccount;
      const params = [from, msg];
      const method = "personal_sign";

      this.provider.sendAsync(
        {
          method,
          params,
          from,
        },
        function (err, result) {
          if (err) return reject(err);
          if (result.error) return reject(result.error);
          const msgParams = { data: msg };
          msgParams.sig = result.result;
          const recovered = sigUtil.recoverPersonalSignature(msgParams);

          if (recovered === from) {
            resolve(result.result);
          }
        }
      );
    });
  };

  personalSignWallet = async ({ message }) => {
    const hexMsg = convertUtf8ToHex(message);
    const msgParams = [hexMsg, this.currentAccount];

    try {
      const result = await this.provider.connector.signPersonalMessage(
        msgParams
      );
      return result;
    } catch (error) {
      console.error(error);
    }
  };

  disconnect = () => {
    localStorage.removeItem("walletconnect");
  };
}

export default new Web3Service();
