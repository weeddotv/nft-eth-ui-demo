import ConfigService from "./ConfigService";
import axios from "axios";

class UserService extends ConfigService {
  updateJwtToken = (jwt) => {
    this.setToken(jwt);
  };

  signOut = () => {
    this.setToken(null);
  };

  getMe = async () => {
    const res = await this.userAxiosInstance.get(`/user/me`);
    return res.data;
  };

  getAccessToken = async () => {
    const res = await axios.get(`/api/shows`);
    return res.data;
  };

  getCurrentSale = async () => {
    const res = await this.userAxiosInstance.get("/sale/currentSale");
    return res.data;
  };
  getRankList = async (size = 20) => {
    const res = await this.userAxiosInstance.post("/user/rank_list", {
      from: 0,
      size: size,
    });
    return res.data;
  };

  getArchive = async (size = 50) => {
    const res = await this.userAxiosInstance.post("/sale/get_archive", {
      from: 0,
      size: size,
    });
    return res.data;
  };

  getProductsSale = async (id, size = 50) => {
    const res = await this.userAxiosInstance.post(`/product/sale`, {
      saleId: id,
      from: 0,
      size: size,
    });
    return res.data;
  };

  getProductByID = async (id) => {
    const res = await this.userAxiosInstance.get(`/product/get/${id}`);
    return res.data;
  };

  onSubscription = async (email) => {
    const res = await this.userAxiosInstance.post(`/subscription`, {
      email: email,
    });
    return res.data;
  };

  transfer = async (values) => {
    const res = await this.userAxiosInstance.post(`/asset/transfer`, {
      ...values,
    });
    return res.data;
  };

  getStashByUserId = async (userId, from = 0, size = 50) => {
    const res = await this.userAxiosInstance.post(`/asset/user`, {
      userId: userId,
      from: from,
      size: size,
    });
    return res.data;
  };

  buy = async (productId, userId) => {
    const res = await this.userAxiosInstance.post(`/product/buy`, {
      productId: productId,
      quantity: 1,
      successUrl: `${
        window.location.origin === "http://localhost:3000"
          ? "https://nft-eth-ui-demo-byc7.vercel.app"
          : window.location.origin
      }/${userId}`,
      cancelUrl: `${
        window.location.origin === "http://localhost:3000"
          ? "https://nft-eth-ui-demo-byc7.vercel.app"
          : window.location.origin
      }`,
    });

    return res.data;
  };

  getUserInfo = async (userId) => {
    const res = await this.userAxiosInstance.get(`/user/info/${userId}`);
    return res.data;
  };

  updateMe = async (data) => {
    const res = await this.userAxiosInstance.post(`/user/update`, data);
    return res.data;
  };
  web3Login = async (data) => {
    const res = await this.userAxiosInstance.post(`/user/connect_address`, data);
    return res.data;
  };
}

export default new UserService();
