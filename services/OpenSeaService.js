import ConfigService from "./ConfigService";
import axios from "axios";

class OpenSeaService {
  constructor() {
    this.openSeaAxios = axios.create({
      baseURL: process.env.NEXT_PUBLIC_OPEN_SEA_ENDPOINT,
      timeout: 99999999,
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
    });
  }

  getAssets = async (searchParams) => {
    const res = await this.openSeaAxios.get(`/assets?${searchParams}`);
    return res.data;
  };
}

export default new OpenSeaService();
