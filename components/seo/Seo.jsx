import Head from "next/head";

const Seo = ({
  seo: {
    title = "Koala Home",
    description = "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    favicon = "/images/favicon.png",
    url = "https://rollingstone.webflow.io/",
    image = "",
    keywords = "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
  },
}) => {
  return (
    <Head>
      <title>{title}</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="title" content={title} />
      <meta name="og:title" content={title} />
      <meta content={title} property="twitter:title" />
      <meta name="referrer" content="always" />
      <meta property="description" content={description} />
      <meta property="og:description" content={description} key="ogdesc" />
      <meta content={description} property="twitter:description" />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={image} key="ogimage" />
      <meta name="keywords" content={keywords} />
      <meta content="summary_large_image" name="twitter:card" />
      <meta property="og:type" content="website" />

      <link href={favicon} rel="shortcut icon" type="image/x-icon" />
      <link href="/images/webclip.png" rel="apple-touch-icon" />
    </Head>
  );
};

export default Seo;
