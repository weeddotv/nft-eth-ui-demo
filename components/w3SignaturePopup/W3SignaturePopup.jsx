import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Web3Service } from "../../services";

const W3SignaturePopupStyled = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: 999;
  background-color: rgba(26, 31, 35, 0.4);
  .cp-b-wrapper {
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    .cp-b-container {
      background: white;
      max-width: 295px;
      width: 90%;
      padding: 24px;
      background: white;
      border-radius: 4px;
      box-shadow: 0 2px 8px 0 rgba(31, 38, 42, 0.2);
      text-align: center;
      img {
        width: 40px;
        margin-bottom: 4px;
      }
      h3 {
        color: #000000;
        font-size: 16px;
        line-height: 1.38;
        font-weight: normal;
        margin-bottom: 16px;
      }
      button{
        padding: 8px 24px;
        background-color: black;
        color: white;
      }
    }
  }
`;

const W3SignaturePopup = ({ confirmPopup, onConnectMetamask }) => {
  const handleWalletSignature = () => {
    Web3Service.confirmed = true;
    onConnectMetamask();
  };

  return (
    <>
      {confirmPopup?.visible ? (
        <W3SignaturePopupStyled>
          <div className="cp-b-wrapper">
            <div className="cp-b-container">
              <img src="/images/metamask.svg" alt="metamask" />
              <h3>You need to sign to verify you wallet</h3>
              <button onClick={handleWalletSignature}>
                <span>Go to sign</span>
              </button>
            </div>
          </div>
        </W3SignaturePopupStyled>
      ) : null}
    </>
  );
};

W3SignaturePopup.prototype = {
  confirmPopup: PropTypes.object,
  onConnectMetamask: PropTypes.func,
};

W3SignaturePopup.defaultProps = {
  confirmPopup: {},
  onConnectMetamask: () => {},
};

export default W3SignaturePopup;
