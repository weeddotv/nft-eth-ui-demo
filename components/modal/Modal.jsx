import React, { useRef } from "react";
import PropTypes from "prop-types";
import { useOnClickOutside } from "../../hooks";

const Modal = ({ visible, onClose, title, desc }) => {
  const ref = useRef();

  useOnClickOutside(ref, () => onClose());
  return (
    <>
      {visible ? (
        <div className="share-popup rs-modal">
          <div className="container-popup is--share">
            <div
              data-w-id="d94334ee-d168-2d58-e895-7d520f758d21"
              className="close-div"
              onClick={onClose}
            >
              <img
                src="/images/CrossSVG.svg"
                loading="lazy"
                alt="Close down send and transfer popup"
              />
            </div>
            {title && <h3 className="heading-10">{title}</h3>}
            <p>{desc}</p>
          </div>
        </div>
      ) : null}
    </>
  );
};

Modal.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  desc: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

Modal.defaultProps = {
  visible: false,
  onClose: () => {},
  title: null,
  desc: null,
};

export default React.memo(Modal);
