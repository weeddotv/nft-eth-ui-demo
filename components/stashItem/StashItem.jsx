import React from "react";
import Link from "next/link";
import { useDispatch } from "../../hooks";
import Router from "next/router";

const StashItem = ({
  imageURL,
  className,
  stashURL,
  stashName,
  remainAmountText,
  description,
  assetId,
  sendStradeOnclick = () => {},
  isOwner = false,
}) => {
  return (
    <div
      data-w-id="639af8a6-1f71-705f-6c24-c287b3390705"
      role="listitem"
      className={`collection-item-10 w-dyn-item ${className}`}
    >
      <a
        href={stashURL}
        target="_blank"
        rel="noreferrer"
        data-w-id="b029e8be-6540-f693-4b0b-041c6525208f"
        className="img-nft-div pack w-inline-block"
      >
        <img
          src={imageURL}
          loading="lazy"
          alt="stash image"
          className="img-nft"
        />
      </a>

      <div className="details">
        <div className="inventory-heading">{stashName}</div>
        {remainAmountText && (
          <div className="inventory-heading is--red">{remainAmountText}</div>
        )}

        {description && <p className="paragraph">{description}</p>}
        {isOwner && (
          <div className="options-div">
            <button className="link" onClick={() => sendStradeOnclick(assetId)}>
              Withdraw
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default StashItem;
