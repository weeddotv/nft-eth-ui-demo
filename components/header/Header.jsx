import Link from "next/link";
import React from "react";
import { useSelector } from "../../hooks";

import { AnchorLink } from "../anchorLink";

const Header = ({ transition = false }) => {
  const { auth } = useSelector((state) => ({
    auth: state.userStore.auth,
  }));

  return (
    <>
      <div className="cursor"></div>
      <div className="html-embed w-embed"></div>

      {transition && (
        <div
          data-w-id="e2226ea1-6ffe-05f6-8a3e-941f6b0552db"
          className="home-transition rs-header-animation-1"
        >
          <div
            data-w-id="8407e882-e239-c915-5d8f-a27c6bfb5f41"
            className="rs-logo-div-transition"
          >
            <img
              src="/images/RSKC-textSVG.svg"
              loading="lazy"
              data-w-id="8407e882-e239-c915-5d8f-a27c6bfb5f42"
              alt=""
              className="logo-circle-logo-transition"
            />
            <img
              src="/images/RSKC-backgroundSVG.svg"
              loading="lazy"
              alt=""
              className="logo-circle-k-centre-transition"
            />
          </div>
          <div className="cover-nft">
            <div
              data-w-id="c8ecaa12-52e4-4767-f44b-b2b39d7f3356"
              className="div-scale rs-header-animation-2"
            >
              <img
                src="/images/RollingStoneAustraliaLogo_RGB_white.svg"
                loading="eager"
                alt=""
                className="image-22"
              />
              <div className="artist-div">
                <img
                  src="/images/K4.png"
                  loading="eager"
                  style={{ display: "none" }}
                  data-w-id="f7783c8e-fe4e-6761-3fe2-ce6fe674a743"
                  srcSet="
            /images/K4-p-500.png   500w,
            /images/K4-p-800.png   800w,
            /images/K4-p-1080.png 1080w,
            /images/K4.png        1118w
          "
                  sizes="100vw"
                  alt=""
                  className="celeb-artist2"
                />
                <img
                  src="/images/K1.png"
                  loading="eager"
                  style={{ display: "block" }}
                  data-w-id="c174029a-c2da-ee38-f0c4-c53f7e686766"
                  srcSet="/images/K1-p-500.png 500w, /images/K1.png 515w"
                  sizes="100vw"
                  alt=""
                  className="celeb-artist"
                />
                <img
                  src="/images/K2.png"
                  loading="eager"
                  data-w-id="477405f9-25fc-8c31-7341-ca0c5fa9f9f5"
                  sizes="100vw"
                  srcSet="/images/K2-p-500.png 500w, /images/K2.png 760w"
                  alt=""
                  className="celeb-artist3"
                />
                <img
                  src="/images/K3.png"
                  loading="eager"
                  data-w-id="aee69b3d-b6d4-e205-9f5f-b133987c2a11"
                  sizes="100vw"
                  srcSet="/images/K3-p-500.png 500w, /images/K3.png 568w"
                  alt=""
                  className="celeb-artist4"
                />
                <img
                  src="/images/K8L.png"
                  loading="eager"
                  data-w-id="00a85e9a-5b62-8eaa-0054-3ed728d08412"
                  sizes="100vw"
                  srcSet="/images/K8L-p-500.png 500w, /images/K8L.png 650w"
                  alt=""
                  className="celeb-artist5"
                />
                <img
                  src="/images/K5.png"
                  loading="eager"
                  data-w-id="47dfcdb6-48cc-e768-2d80-3c03424e812b"
                  sizes="100vw"
                  srcSet="/images/K5-p-500.png 500w, /images/K5.png 702w"
                  alt=""
                  className="celeb-artist6"
                />
                <img
                  src="/images/K6.png"
                  loading="eager"
                  data-w-id="b5b1e8a9-a686-e592-3e58-77aafdcd819a"
                  sizes="100vw"
                  srcSet="
            /images/K6-p-500.png  500w,
            /images/K6-p-800.png  800w,
            /images/K6.png       1050w
          "
                  alt=""
                  className="celeb-artist7"
                />
                <img
                  src="/images/K7.png"
                  loading="eager"
                  data-w-id="4824ae37-d3fd-40fd-3411-eea0db227db2"
                  sizes="100vw"
                  srcSet="
            /images/K7-p-500.png 500w,
            /images/K7-p-800.png 800w,
            /images/K7.png       935w
          "
                  alt=""
                  className="celeb-artist8"
                />
              </div>
            </div>
          </div>
        </div>
      )}

      <div className="nav-menu">
        <div className="nav-hold">
          <div id="navBack" className="mobile-back"></div>
          <div className="desktop-menu">
            <div>
              <Link href="/">
                <a
                  aria-current="page"
                  className="rs-logo-div w-inline-block w--current"
                >
                  <img
                    src="/images/RSKC-text-blackSVG.svg"
                    loading="lazy"
                    alt=""
                    className="logo-circle-logo"
                  />
                  <img
                    src="/images/RSKC-background-whiteSVG.svg"
                    loading="lazy"
                    alt=""
                    className="logo-circle-k-centre"
                  />
                </a>
              </Link>
            </div>
            <div className="link-nav-div">
              <AnchorLink
                href="/#live-now"
                data-w-id="c013ba9e-dc19-8be9-5d74-694db6b7cdd1"
                className="button-kt w-inline-block"
              >
                <p className="paragraph-16">ADOPT A KOALA</p>
              </AnchorLink>

              <div className="mobile-menu">
                {auth && (
                  <div className="menu-contents">
                    <div className="red-line"></div>
                    <AnchorLink
                      href={`/${auth?.userId}`}
                      className="link-block-2 w-inline-block"
                    >
                      <div className="menu-text">YOUR STASH</div>
                    </AnchorLink>
                  </div>
                )}

                <div className="menu-contents">
                  <div className="red-line"></div>
                  <Link href="/leaderboard">
                    <a className="link-block-2 w-inline-block">
                      <div className="menu-text">LEADERBOARD</div>
                    </a>
                  </Link>
                </div>

                {!auth ? (
                  <div className="menu-contents">
                    <div className="red-line"></div>
                    <AnchorLink
                      href="/api/auth/login"
                      className="link-block-2 w-inline-block"
                    >
                      <div className="menu-text">LOGIN</div>
                    </AnchorLink>
                  </div>
                ) : (
                  <>
                    <div className="menu-contents">
                      <div className="red-line"></div>
                      <Link href="/account">
                        <a className="link-block-2 w-inline-block">
                          <div className="menu-text">YOUR ACCOUNT</div>
                        </a>
                      </Link>
                    </div>
                    <div className="menu-contents">
                      <div className="red-line"></div>
                      <AnchorLink
                        href="/api/auth/logout"
                        className="link-block-2 w-inline-block"
                      >
                        <div className="menu-text">LOGOUT</div>
                      </AnchorLink>
                    </div>
                  </>
                )}
              </div>
              <div
                data-w-id="1b388a7d-206b-4440-ae63-2a83684c2370"
                className="hamb-circle"
              >
                <div className="hamb-white-back">
                  <img
                    src="/images/hamb-line-threeSVG.svg"
                    loading="lazy"
                    alt=""
                    className="image-26"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
