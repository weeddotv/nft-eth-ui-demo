import React from "react";
import Link from "next/link";

const LeaderBoardItem = ({
  imageURL = "/images/defaultAvatar.svg",
  username,
  name,
  instagramURL,
  twitterURL,
  score,
  rank,
  stashURL,
}) => {
  return (
    <div className="w-layout-grid grid">
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d45980-8181540e"
        className="grid-hold"
      >
        <img
          src={imageURL}
          loading="lazy"
          alt="Avatar for user account page"
          className="image-profile"
        />
      </div>
      <div className="grid-hold">
        <div className="name">
          <div className="leadertext">@{username || name}</div>
        </div>
      </div>
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d45986-8181540e"
        className="grid-hold is--right"
        style={{ opacity: "0", visibility: "hidden" }}
      >
        <a
          href={instagramURL}
          target="_blank"
          rel="noreferrer"
          className="link-block-4 is--lessspace w-inline-block"
        >
          <img
            src="/images/Instagram.svg"
            loading="lazy"
            alt="Instagram icon"
            className="leader-icon"
          />
        </a>
      </div>
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d45989-8181540e"
        className="grid-hold"
        style={{ opacity: "0", visibility: "hidden" }}
      >
        <a
          href={twitterURL}
          target="_blank"
          rel="noreferrer"
          className="link-block-4 is--lessspace w-inline-block"
        >
          <img
            src="/images/Twitter.svg"
            loading="lazy"
            alt="Twitter icon"
            className="leader-icon"
          />
        </a>
      </div>
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d4598c-8181540e"
        className="grid-hold"
      >
        <div className="div-hold-leader">
          <img
            src="/images/Collect.svg"
            loading="lazy"
            alt="Collectors score item"
            className="image-13"
          />
          <div className="leadertext">Score:</div>
          <div className="leadertext is--score">{score}</div>
        </div>
      </div>
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d45993-8181540e"
        className="grid-hold"
      >
        <div className="div-hold-leader">
          <img
            src="/images/Medal.svg"
            loading="lazy"
            alt="Medal item"
            className="image-13"
          />
          <div className="leadertext">Rank:</div>
          <div className="leadertext is--score">{rank}</div>
        </div>
      </div>
      <div
        id="w-node-_46fa6824-2f9a-c122-2aba-2a4c91d4599a-8181540e"
        className="grid-hold is--right"
      >
        <p className="paragraph-4">
          <Link href={stashURL}>
            <a data-w-id="46fa6824-2f9a-c122-2aba-2a4c91d4599c">View Stash</a>
          </Link>
        </p>
      </div>
    </div>
  );
};

export default LeaderBoardItem;
