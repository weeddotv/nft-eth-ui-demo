import React from "react";
import { Field, Form } from "react-final-form";

const SendTradePopup = ({ onWithdraw, onClose, onSendLoading }) => {
  return (
    <div className="send-trade-popup isOpen">
      <div className="container-popup">
        <div
          data-w-id="93e2d941-c374-c8b4-f45c-739de4a07fda"
          className="close-div"
          onClick={onClose}
        >
          <img
            src="/images/CrossSVG.svg"
            loading="lazy"
            alt="Close down send and transfer popup"
          />
        </div>

        <h3 className="heading-10">Withdraw</h3>
        <p>
          Send this NFT to your personal wallet. From here you can list this NFT
          for sale on marketplaces like{" "}
          <a href="https://opensea.io/" target="_blank" rel="noreferrer">
            OpenSea
          </a>
        </p>
        <div className="form-block is--popup w-form">
          <Form
            onSubmit={onWithdraw}
            render={({ handleSubmit }) => (
              <form className="form" onSubmit={handleSubmit}>
                <Field name="to">
                  {({ input }) => (
                    <>
                      <input
                        type="text"
                        className="form-feild w-input"
                        maxLength="256"
                        name="WAXWallet"
                        placeholder="Enter Your Wallet"
                        {...input}
                      />
                    </>
                  )}
                </Field>

                <input
                  type="submit"
                  disabled={onSendLoading}
                  value={!onSendLoading ? "SUBMIT" : "Please wait..."}
                  className="submit-button w-button"
                />
              </form>
            )}
          />
        </div>

        {/* {!withDrawSuccess ? (
          <>
            <h3 className="heading-10">Withdraw</h3>
            <p>
              Send this NFT to your personal wallet. From here you can list this
              NFT for sale on marketplaces like{" "}
              <a href="https://opensea.io/" target="_blank" rel="noreferrer">
                OpenSea
              </a>
            </p>
            <div className="form-block is--popup w-form">
              <Form
                onSubmit={onWithdraw}
                render={({ handleSubmit }) => (
                  <form className="form" onSubmit={handleSubmit}>
                    <Field name="to">
                      {({ input }) => (
                        <>
                          <input
                            type="text"
                            className="form-feild w-input"
                            maxLength="256"
                            name="WAXWallet"
                            placeholder="Enter Your Wallet"
                            {...input}
                          />
                        </>
                      )}
                    </Field>

                    <input
                      type="submit"
                      disabled={onSendLoading}
                      value={!onSendLoading ? "SUBMIT" : "Please wait..."}
                      className="submit-button w-button"
                    />
                  </form>
                )}
              />
            </div>
          </>
        ) : (
          <>
            <h3 className="heading-10">Success</h3>

            <p style={{ textAlign: "left" }}>
              We've received your request to transfer your NFT to your personal
              wallet. It might take a few minutes to complete. Please check the
              withdraw status here: (
              <a
                href="https://testnets.opensea.io/assets/goerli/0x0861730a67bac0ab8563fa8cec89770ddcc2c55e/12"
                target="_blank"
                rel="noreferrer"
              >
                https://testnets.opensea.io/assets/goerli/0x0861730a67bac0ab8563fa8cec89770ddcc2c55e/12
              </a>
              ). From your wallet you can list the NFT for sale on marketplaces
              like{" "}
              <a href="https://opensea.io/" target="_blank" rel="noreferrer">
                OpenSea
              </a>{" "}
              Send this NFT to your personal wallet. From here you can list this
              NFT for sale on marketplaces like{" "}
            </p>
          </>
        )} */}
      </div>
    </div>
  );
};

export default SendTradePopup;
