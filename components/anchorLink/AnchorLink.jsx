import React from "react";

const AnchorLink = ({ children, href, className, ...rest }) => {
  return (
    <a href={href} className={className} {...rest}>
      {children}
    </a>
  );
};

export default AnchorLink;
