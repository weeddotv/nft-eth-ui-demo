import React, { useState } from "react";
import { copyToClipboard } from "../../utils/global";

const SharePopup = ({ url, onClose }) => {
  const [copied, setCopied] = useState(false);

  const onCopy = () => {
    copyToClipboard(url);
    setCopied(true);
  };
  return (
    <div className="share-popup isOpen">
      <div className="container-popup is--share">
        <div
          data-w-id="d94334ee-d168-2d58-e895-7d520f758d21"
          className="close-div"
          onClick={onClose}
        >
          <img
            src="/images/CrossSVG.svg"
            loading="lazy"
            alt="Close down send and transfer popup"
          />
        </div>
        <h3 className="heading-10">Share</h3>
        <p>
          Copy this link and share it on your favourite social media site. For
          example, your instagram bio.
        </p>
        <div className="share-url">{url}</div>
        <button onClick={onCopy} className="link">
          {!copied ? "Copy url" : "Copied"}
        </button>
      </div>
    </div>
  );
};

export default SharePopup;
