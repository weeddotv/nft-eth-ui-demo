import React from "react";
import Link from "next/link";
import { Form, Field } from "react-final-form";
import { useState } from "react";
import { useDispatch, useSelector } from "../../hooks/useRematch";

const Footer = () => {
  const [subscribed, setSubscribed] = useState(false);
  const [subscriptionError, setSubscriptionError] = useState(false);

  const { subscriptionLoading } = useSelector((state) => ({
    subscriptionLoading: state?.loading.effects.userStore.subscription,
  }));

  const { subscription } = useDispatch((state) => ({
    subscription: state?.userStore.subscription,
  }));

  const onSubscription = async (values) => {
    try {
      await subscription(values);
      setSubscribed(true);
    } catch (e) {
      setSubscriptionError(true);
    }
  };
  return (
    <>
      <div className="footer">
        <div className="container is--wider">
          <div className="red-footer">
            <div className="footer-top-div">
              <h2 className="k-h2-white">Never miss a new celeb koala:</h2>
              <div className="footer-form w-form">
                {!subscribed ? (
                  <Form
                    onSubmit={onSubscription}
                    render={({ handleSubmit }) => (
                      <form
                        name="email-form"
                        data-name="Email Form"
                        className="form"
                        onSubmit={handleSubmit}
                      >
                        <Field name="email">
                          {({ input }) => (
                            <>
                              <input
                                type="email"
                                className="form-feild w-input"
                                maxLength="256"
                                name="name-2"
                                data-name="Name 2"
                                placeholder="Your email"
                                id="name-2"
                                {...input}
                              />
                            </>
                          )}
                        </Field>

                        <input
                          type="submit"
                          value={
                            !subscriptionLoading
                              ? "GET ALERTED"
                              : "Please wait..."
                          }
                          className="submit-button w-button"
                        />
                      </form>
                    )}
                  />
                ) : (
                  <div
                    className="success-message w-form-done"
                    style={{ display: "block" }}
                  >
                    <div className="error-text">
                      Thank you! You&#x27;ll hear from us soon.
                    </div>
                  </div>
                )}

                {subscriptionError && (
                  <div className="error-message-2 w-form-fail">
                    <div className="error-text">
                      Hmmm, something went wrong. Please try again!
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="footer-bottom-div">
              <div className="div-block-37">
                <a
                  href="https://au.rollingstone.com"
                  target="_blank"
                  rel="noreferrer"
                  className="link-block-5 w-inline-block"
                >
                  <img
                    src="/images/RollingStoneAustraliaLogo_RGB_white.svg"
                    loading="lazy"
                    alt=""
                    className="image-20"
                  />
                </a>
                <div className="div-block-43">
                  <a
                    href="https://twitter.com/rollingstoneaus"
                    target="_blank"
                    rel="noreferrer"
                    className="link-block-6 w-inline-block"
                  >
                    <img src="/images/TW.svg" loading="lazy" alt="" />
                  </a>
                  <a
                    href="https://www.facebook.com/rollingstoneaustralia/"
                    target="_blank"
                    rel="noreferrer"
                    className="link-block-6 w-inline-block"
                  >
                    <img src="/images/FB.svg" loading="lazy" alt="" />
                  </a>
                  <a
                    href="https://www.instagram.com/rollingstoneaus/"
                    target="_blank"
                    rel="noreferrer"
                    className="link-block-6 w-inline-block"
                  >
                    <img src="/images/IN.svg" loading="lazy" alt="" />
                  </a>
                </div>
                <div className="div-block-44">
                  <p className="kpara is-white">NFT Studio:</p>
                  <a
                    href="https://scarce.studio"
                    target="_blank"
                    rel="noreferrer"
                    className="link-8"
                  >
                    website
                  </a>
                </div>
                <a
                  href="https://scarce.studio"
                  target="_blank"
                  rel="noreferrer"
                  className="w-inline-block"
                >
                  <img
                    src="/images/Scarce-whiteSVG.svg"
                    loading="lazy"
                    alt=""
                    className="image-31"
                  />
                </a>
              </div>
              <div>
                <Link href="/about-us/about-us">
                  <a className="link-5 is--left">ABOUT</a>
                </Link>
                <Link href="/cmsfaqs/faq-content">
                  <a className="link-5 is--left">FAQ</a>
                </Link>
                <Link href="/cmsterms/terms">
                  <a className="link-5">TERMS</a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
