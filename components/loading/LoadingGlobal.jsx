import React from "react";
import styled from "styled-components";
const LoadingStyled = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 111;
  background-color: rgba(255, 255, 255, 0.6);
  z-index: -1;
  opacity: 0;
  visibility: hidden;

  &.isActive {
    z-index: 111;
    opacity: 1;
    visibility: visible;
  }
`;

const LoadingGlobal = ({ className }) => {
  return (
    <LoadingStyled className={className}>
      <img
        src="/images/RS-RecordSVG.svg"
        loading="lazy"
        data-w-id="f032ea90-42d3-2ab5-224e-b233f0d003c6"
        alt="A Rolling Stone Australia record"
      />
    </LoadingStyled>
  );
};

export default LoadingGlobal;
