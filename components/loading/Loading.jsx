import React from "react";
import styled from "styled-components";
const LoadingStyled = styled.div`
  padding: 80px 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    margin: 0 auto;
  }
`;

const Loading = ({ minHeight = "auto" }) => {
  return (
    <LoadingStyled style={{ minHeight: minHeight }}>
      <img
        src="/images/RS-RecordSVG.svg"
        loading="lazy"
        data-w-id="f032ea90-42d3-2ab5-224e-b233f0d003c6"
        alt="A Rolling Stone Australia record"
      />
    </LoadingStyled>
  );
};

export default Loading;
