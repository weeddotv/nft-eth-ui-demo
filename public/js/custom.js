$(document).ready(function () {
  init();

  function init() {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }

    $(".hamb").click(function () {
      var clicks = $(this).data("clicks");
      if (clicks) {
        $("body").css("overflow", "auto");
      } else {
        $("body").css("overflow", "hidden");
      }
      $(this).data("clicks", !clicks);
    });
    $(".navBack").click(function () {
      $(".hamb").click();
    });

    setFontSize();
    window.addEventListener("resize", function () {
      setFontSize();
    });
  }

  function setFontSize() {
    let maxWidth = 1920;
    let windowWidth = $(window).width();
    if (windowWidth >= maxWidth || windowWidth < 992) {
      $("body").removeAttr("style");
    } else {
      let fontSize = windowWidth / 100 / 16;
      $("body").css("font-size", fontSize + "rem");
    }
  }
});
