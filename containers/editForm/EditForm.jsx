import React from "react";
import { Field, Form } from "react-final-form";

const EditForm = ({ onUpdate, closeEditPopup, loading, user }) => {
  return (
    <div className={`send-trade-popup edit-popup isOpen`}>
      <div className="container-popup">
        <div
          data-w-id="93e2d941-c374-c8b4-f45c-739de4a07fda"
          className="close-div"
          onClick={closeEditPopup}
        >
          <img
            src="/images/CrossSVG.svg"
            loading="lazy"
            alt="Close down send and transfer popup"
          />
        </div>
        <h3 className="heading-10">Edit</h3>
        <div className="form-block is--popup w-form">
          <Form
            onSubmit={onUpdate}
            initialValues={{
              username: user?.username,
              address: user?.address,
              twitter: user?.twitter,
              instagram: user?.instagram,
            }}
            render={({ handleSubmit }) => (
              <form className="rs-form" onSubmit={handleSubmit}>
                <Field name="username">
                  {({ input }) => (
                    <div className="rs-form-field">
                      <h6>User name</h6>
                      <input
                        type="text"
                        className="form-feild w-input"
                        maxLength="256"
                        placeholder="user name"
                        {...input}
                      />
                    </div>
                  )}
                </Field>
                <Field name="address">
                  {({ input }) => (
                    <div className="rs-form-field">
                      <h6>Address</h6>
                      <input
                        type="text"
                        className="form-feild w-input"
                        maxLength="256"
                        placeholder="Address"
                        {...input}
                      />
                    </div>
                  )}
                </Field>
                <Field name="twitter">
                  {({ input }) => (
                    <div className="rs-form-field">
                      <h6>Twitter</h6>

                      <input
                        type="text"
                        className="form-feild w-input"
                        maxLength="256"
                        placeholder="Twitter"
                        {...input}
                      />
                    </div>
                  )}
                </Field>

                <Field name="instagram">
                  {({ input }) => (
                    <div className="rs-form-field">
                      <h6>Instagram</h6>
                      <input
                        type="text"
                        className="form-feild w-input"
                        maxLength="256"
                        placeholder="Instagram"
                        {...input}
                      />
                    </div>
                  )}
                </Field>
                <input
                  type="submit"
                  disabled={loading}
                  value={!loading ? "SUBMIT" : "Please wait..."}
                  className="submit-button w-button"
                />
              </form>
            )}
          />
        </div>
      </div>
    </div>
  );
};

export default EditForm;
