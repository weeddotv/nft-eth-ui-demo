import React, { useEffect } from "react";
import { Footer, Header, Modal, Seo, W3SignaturePopup } from "../components";
import styled from "styled-components";
import { useUser } from "@auth0/nextjs-auth0";
import { useDispatch, useSelector } from "../hooks/useRematch";

const LayoutStyled = styled.main``;

const DefaultLayout = ({
  seo = {},
  transition,
  showFooter = true,
  children,
}) => {
  const { user, isLoading } = useUser();

  const { modal, confirmPopup } = useSelector((state) => ({
    modal: state.appStore.modal,
    confirmPopup: state.userStore.confirmPopup,
  }));

  const { getMe, closeModal, setAuth, connectMetamask } = useDispatch(
    (state) => ({
      getMe: state.userStore.getMe,
      closeModal: state.appStore.closeModal,
      setAuth: state.userStore.setAuth,
      connectMetamask: state.userStore.connectMetamask,
    })
  );

  const onClose = () => {
    closeModal();
  };

  useEffect(() => {
    if (user && !isLoading) {
      setAuth(user);
      getMe();
    }
  }, [user, getMe, isLoading, setAuth]);

  return (
    <LayoutStyled>
      <Seo seo={seo} />

      <Header transition={transition} user={user} isLoading={isLoading} />
      {children}
      {showFooter && <Footer />}

      <Modal onClose={onClose} {...modal} />
      <W3SignaturePopup
        confirmPopup={confirmPopup}
        onConnectMetamask={connectMetamask}
      />
    </LayoutStyled>
  );
};

export default DefaultLayout;
