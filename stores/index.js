import { init } from "@rematch/core";
import loadingPlugin from "@rematch/loading";
import selectPlugin from "@rematch/select";

import * as models from "./models";

const store = init({
  plugins: [loadingPlugin(), selectPlugin()],
  models,
});

export default store;
