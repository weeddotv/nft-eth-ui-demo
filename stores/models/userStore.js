import { OpenSeaService, UserService, Web3Service } from "../../services";
import { handleError } from "../../utils/handleError";

export const userStore = {
  state: {
    currentSale: null,
    archive: null,
    auth: null,
    userInfo: null,
    stash: null,
    confirmPopup: {
      visible: false,
    },
  },
  reducers: {
    setCurrentSale: (state, payload) => {
      return {
        ...state,
        currentSale: payload,
      };
    },
    setArchive: (state, payload) => {
      return {
        ...state,
        archive: payload,
      };
    },
    setAuth: (state, payload) => {
      return {
        ...state,
        auth: payload,
      };
    },
    updateAuth: (state, payload) => {
      return {
        ...state,
        auth: { ...state.auth, ...payload },
      };
    },
    setUserInfo: (state, payload) => {
      return {
        ...state,
        userInfo: payload,
      };
    },
    setStash: (state, payload) => {
      return {
        ...state,
        stash: payload,
      };
    },
    setConfirmPopup(state, payload) {
      return {
        ...state,
        confirmPopup: payload,
      };
    },
  },
  effects: (dispatch) => ({
    async getMe() {
      try {
        const res = await UserService.getAccessToken();
        UserService.setToken(res?.accessToken);
        const user = await UserService.getMe();
        this.updateAuth(user);
      } catch (e) {
        // console.error("--- Token is not valid, set user to not login:", e);
        UserService.setToken(null);
        this.setAuth(null);
        window.location.assign("/api/auth/logout");
        handleError(e);
      }
    },
    async updateMe(payload) {
      try {
        const user = await UserService.updateMe({ ...payload });
        this.updateAuth(user);
      } catch (e) {
        console.error("--- updateMe error:", e);
        handleError(e);
      }
    },

    async getCurrentSale() {
      try {
        const collection = await UserService.getCurrentSale();
        const productSale = await UserService.getProductsSale(collection?._id);
        this.setCurrentSale({
          ...collection,
          products: productSale?.content,
        });
      } catch (e) {
        console.error("Error: getCollectionSale", e);
        handleError(e);
      }
    },
    async getArchive(payload) {
      const { pageSize } = payload;
      try {
        const res = await UserService.getArchive(pageSize);
        this.setCurrentSale(res?.content);
      } catch (e) {
        console.error("Error: getArchive", e);
        handleError(e);
      }
    },
    async buyNFT(payload) {
      const { productId, userId } = payload;
      try {
        const res = await UserService.buy(productId, userId);
        window.location.assign(res?.url);
      } catch (e) {
        console.error("Error: buyNFT", e);
        handleError(e);
      }
    },
    async subscription(payload) {
      try {
        const { email } = payload;
        await UserService.onSubscription(email);
      } catch (e) {
        console.log("error subscription", e);
        handleError(e);
      }
    },
    async getStashByUserId({ userId }) {
      try {
        let stash = await UserService.getStashByUserId(userId);
        const firstItem = stash?.content[0];
        if (firstItem) {
          const searchParams = new URLSearchParams();
          searchParams.set("limit", 20);
          searchParams.set("offset", 0);
          searchParams.set("include_orders", "false");
          searchParams.set("order_direction", "desc");
          searchParams.set(
            "asset_contract_address",
            firstItem[0]?.erc721Contract
          );

          stash?.content.forEach((item) => {
            searchParams.append("token_ids", item[0].tokenId);
          });

          const openSeaAssetsRes = await OpenSeaService.getAssets(
            searchParams.toString()
          );

          stash = stash.content.map((item) => {
            return {
              ...item[0],
              openSeaAssets: openSeaAssetsRes.assets.filter(
                (openSeaAssetItem) =>
                  item[0].tokenId === openSeaAssetItem.token_id
              )[0],
            };
          });

          this.setStash(stash);
        } else {
          this.setStash([]);
        }
      } catch (e) {
        console.error("Error: getStashByUserId", e);
        handleError(e);
      }
    },
    async getUserInfo({ userId }) {
      try {
        const userRes = await UserService.getUserInfo(userId);
        this.setUserInfo(userRes);
      } catch (e) {
        console.error("Error: getUserInfo", e);
      }
    },
    async onSend({ to, id }, { userStore }) {
      try {
        await UserService.transfer({ to, id });
        const transferItem = userStore?.stash?.find((item) => item._id === id);
        this.setStash(userStore?.stash?.filter((item) => item._id !== id));

        dispatch.appStore.openModal({
          title: "Success",
          desc: (
            <>
              We've received your request to transfer your NFT to your personal
              wallet. It might take a few minutes to complete. Please check the
              withdraw status here: (
              <a
                href={transferItem?.openSeaAssets?.permalink}
                target="_blank"
                rel="noreferrer"
              >
                {transferItem?.openSeaAssets?.permalink}
              </a>
              ). From your wallet you can list the NFT for sale on marketplaces
              like{" "}
              <a href="https://opensea.io/" target="_blank" rel="noreferrer">
                OpenSea
              </a>
            </>
          ),
        });
      } catch (err) {
        console.error("Error: onSend", e);
        handleError(e);
      }
    },
    async connectMetamask() {
      try {
        await Web3Service.initialize();
        let signature = "";
        let message = "";

        if (!window.ethereum) {
          if (!Web3Service.confirmed) {
            await Web3Service.connectWallet();
            this.setConfirmPopup({
              visible: true,
            });
          } else {
            this.setConfirmPopup({
              visible: false,
            });
            message = JSON.stringify({
              address: Web3Service.currentAccount,
              currentTime: Date.now(),
            });
            signature = await Web3Service.personalSignWallet({
              message: message,
            });
          }
        } else {
          Web3Service.confirmed = true;
          await Web3Service.connect();
          message = JSON.stringify({
            address: Web3Service.currentAccount,
            currentTime: Date.now(),
          });
          signature = await Web3Service.personalSign({
            message: message,
          });
        }
        if (Web3Service.confirmed) {
          await UserService.web3Login({
            message: message,
            signature: signature,
          });
          this.getMe();
        }
      } catch (e) {
        handleError(e);
      }
    },
  }),
};
