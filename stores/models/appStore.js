import { UserService } from "../../services";
import { MODAL_TYPE } from "../../utils/constants";

const initState = {
  modal: {
    visible: false,
    title: "",
    desc: "",
  },
  rankList: null,
};

export const appStore = {
  state: initState,
  reducers: {
    openModal(state, payload) {
      return {
        ...state,
        modal: {
          visible: true,
          ...payload,
        },
      };
    },
    closeModal(state) {
      return {
        ...state,
        modal: initState.modal,
      };
    },
    setRankList(state, payload) {
      return {
        ...state,
        rankList: payload,
      };
    },
  },
  effects: () => ({
    async getRankList(payload) {
      try {
        const { pageSize } = payload;
        const res = await UserService.getRankList(pageSize);
        this.setRankList(res.content);
      } catch (e) {
        console.log("error getRankList", e);
      }
    },
  }),
};
