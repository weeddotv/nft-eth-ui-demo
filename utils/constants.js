export const MODAL_TYPE = {
  NOTIFY: "notify",
  ERROR: "error",
  CONFIRM: "confirm",
};

export const AUTH_TOKEN = "auth-token";
