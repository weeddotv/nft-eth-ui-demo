import * as encoding from '@walletconnect/encoding';

export const convertUtf8ToHex = (utf8, noPrefix) => {
  return encoding.utf8ToHex(utf8, !noPrefix);
};

export const toHex = d => {
  return ('0' + Number(d).toString(16)).slice(-2).toUpperCase();
};

export const shortAddress = address => {
  if (address) {
    return (
      address?.slice(0, 5) +
      '...' +
      address?.slice(address.length - 4, address.length)
    );
  }
  return null;
};

