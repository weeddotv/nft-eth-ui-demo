import store from "../stores";

export const handleError = (error) => {
  try {
    store.dispatch.appStore.openModal({
      desc: error?.response?.data || error?.message,
    });
  } catch (e) {
    console.log("handleError Error:", e);
  }
};
