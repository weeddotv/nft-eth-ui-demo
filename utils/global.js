import React from "react";
export const formatText = (text) =>
  text?.split("\n").map((s, i) => (
    <React.Fragment key={i}>
      <p>{s}</p>
    </React.Fragment>
  ));

export const formatListText = (text) => (
  <ul role="list">
    {text?.split("\n").map((s, i) => (
      <li key={i}>{s}</li>
    ))}
  </ul>
);

export const formatIPFSImage = (url) => {
  if (url) {
    return `https://ipfs.io/ipfs/${url}`;
  }

  return null;
};

export const copyToClipboard = (str) => {
  const el = document.createElement("textarea");
  el.value = str;
  el.setAttribute("readonly", "");
  el.style.position = "absolute";
  el.style.left = "-9999rem";
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
};
