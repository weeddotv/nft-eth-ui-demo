import Responsive from 'react-responsive';

export const Mobile = (props) => <Responsive {...props} maxWidth={600} />;
export const Tablet = (props) => <Responsive {...props} minWidth={601} />;
export const Desktop = (props) => <Responsive {...props} minWidth={991} />;
export const NotDesktop = (props) => <Responsive {...props} maxWidth={990} />;

export const TabletOnly = (props) => <Responsive {...props} minWidth={601} maxWidth={1080} />;
