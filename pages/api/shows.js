import { getAccessToken, withApiAuthRequired } from "@auth0/nextjs-auth0";

// export default withApiAuthRequired(async function shows(req, res) {
//   try {
//     const { accessToken } = await getAccessToken(req, res, {
//       // scopes: ['openid', 'profile', 'email', 'offline_access', 'read:shows']
//       scopes: ["email", "profile", "openid", "offline_access"],
//     });
//     const apiPort = process.env.API_PORT || 3001;
//     const response = await fetch(`http://localhost:${apiPort}/api/shows`, {
//       headers: {
//         Authorization: `Bearer ${accessToken}`,
//       },
//     });
//     const shows = await response.json();
//     res.status(200).json({ accessToken });
//   } catch (error) {
//     res.status(error.status || 500).json({ error: error.message });
//   }
// });

export default withApiAuthRequired(async function shows(req, res) {
  try {
    const { accessToken } = await getAccessToken(req, res, {
      scopes: ["email", "profile", "openid"],
    });
    res.status(200).json({ accessToken });
  } catch (error) {
    res.status(error.status || 500).json({ error: error.message });
  }
});
