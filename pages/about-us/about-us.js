import { DefaultLayout } from "../../layouts";

export default function AboutUs({seo}) {
  return (
    <DefaultLayout seo={seo}>
      <div
        data-w-id="775c3b0e-230d-f1c3-de87-8746883e0de6"
        className="overflow"
      >
        <div className="section">
          <div className="container is--left is--hero">
            <h1>About us</h1>
            <div className="rich-text-block-3 w-richtext"></div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}

AboutUs.getInitialProps = async () => {
  return {
    seo: {
      title: "About us | Rolling Stone NFT",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
