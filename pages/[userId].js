import { StashItem, Loading, SendTradePopup } from "../components";
import { DefaultLayout } from "../layouts";
import { useDispatch, useSelector } from "../hooks";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { Form, Field } from "react-final-form";
import { OpenSeaService, UserService } from "../services";
import { SharePopup } from "../components";

export default function Stash({ stashInfo, notFound }) {
  const Router = useRouter();
  const [sendTradeData, setSendTradeData] = useState(null);
  const [shareData, setShareData] = useState(null);
  const [isOwner, setIsOwner] = useState(false);

  const { stash, auth, getStashByUserIdLoading, onSendLoading, userInfo } =
    useSelector((state) => ({
      stash: state.userStore.stash,
      auth: state.userStore.auth,
      userInfo: state.userStore.userInfo,
      getStashByUserIdLoading: state.loading.effects.userStore.getStashByUserId,
      onSendLoading: state.loading.effects.userStore.onSend,
    }));

  const { setStash, onSend, getUserInfo } = useDispatch((state) => ({
    getStashByUserId: state.userStore.getStashByUserId,
    onSend: state.userStore.onSend,
    setStash: state.userStore.setStash,
    getUserInfo: state.userStore.getUserInfo,
  }));

  useEffect(() => {
    if (notFound) {
      Router.push("/404");
    }
  }, [notFound, Router]);

  useEffect(() => {
    if (stashInfo) {
      setStash(stashInfo);
    }
  }, [stashInfo, setStash]);

  useEffect(() => {
    if (Router?.query?.userId) {
      getUserInfo({ userId: Router?.query?.userId });
    }
  }, [Router?.query?.userId, getUserInfo]);

  useEffect(() => {
    if (Router?.query?.userId === auth?.userId) {
      setIsOwner(true);
    } else {
      setIsOwner(false);
    }
  }, [auth, Router]);

  const openSendPopup = (assetId) => {
    setSendTradeData({
      ...sendTradeData,
      assetId: assetId,
      isOpen: true,
    });
  };

  const onClose = () => {
    setSendTradeData({
      isOpen: false,
    });
  };

  const onShare = () => {
    setShareData({
      isOpen: true,
      url: window.location.href,
    });
  };
  const onShareClose = () => {
    setShareData({
      isOpen: false,
      url: null,
    });
  };

  const onWithdraw = async (values) => {
    await onSend({
      id: sendTradeData?.assetId,
      ...values,
    });
    onClose();
  };

  return (
    <DefaultLayout
      seo={{
        title: stashInfo?.[0]?.openSeaAssets?.name,
        image: stashInfo?.[0]?.openSeaAssets?.image_url,
      }}
    >
      <div
        data-w-id="37488e27-ec65-d120-bdcc-813c5eff73d9"
        className="overflow rol-s-stash"
      >
        <div className="section">
          {sendTradeData?.isOpen && (
            <SendTradePopup
              onClose={onClose}
              onWithdraw={onWithdraw}
              onSendLoading={onSendLoading}
            />
          )}
          {shareData?.isOpen && (
            <SharePopup url={shareData?.url} onClose={onShareClose} />
          )}
          <div className="container is-centre is--bottom">
            <div className="centre-contents">
              <div className="picture-div">
                <img
                  src={
                    isOwner
                      ? auth?.avatar
                      : userInfo?.avatar || "/images/defaultAvatar.svg"
                  }
                  loading="lazy"
                  alt="Avatar for user account page"
                  className="image-8 is---space"
                />
              </div>
              <div className="personal-collect">
                <div className="personal-div">
                  <p className="paragraph-4">
                    @
                    {isOwner
                      ? auth?.username || auth?.name
                      : userInfo?.username || userInfo?.name}
                  </p>
                  <p className="paragraph-4">|</p>
                  <p className="paragraph-4">
                    <button
                      onClick={onShare}
                      data-w-id="4b1e4320-c401-8623-cd9b-caebe0fb6887"
                      className="rs-link"
                    >
                      share
                    </button>
                  </p>
                </div>
                <div className="personal-div">
                  <Link href="/leaderboard">
                    <a className="collectors2 w-inline-block">
                      <img
                        src="/images/Collect.svg"
                        loading="lazy"
                        alt="Collectors score item"
                        className="image-13"
                      />
                      <div className="text-block-2">Collector score:</div>
                    </a>
                  </Link>

                  <p className="paragraph-4">
                    <Link href="/leaderboard">
                      <a data-w-id="f84c6852-6b84-f4b4-95e1-26fa20d0d65b">
                        {isOwner ? auth?.score : userInfo?.score}
                      </a>
                    </Link>
                  </p>
                </div>
              </div>
              <h1 className="heading-9 is--hero">NFT Stash</h1>
            </div>

            {getStashByUserIdLoading ? (
              <Loading />
            ) : (
              <div className="collection-list-wrapper-6 w-dyn-list">
                <div role="list" className="collection-list-7 w-dyn-items">
                  {stash?.map((item) => (
                    <StashItem
                      key={item?._id}
                      imageURL={item?.openSeaAssets?.image_url}
                      stashURL={item?.openSeaAssets?.permalink}
                      stashName={item?.openSeaAssets?.name}
                      description={item?.openSeaAssets?.description}
                      assetId={item?._id}
                      sendStradeOnclick={openSendPopup}
                      isOwner={isOwner}
                    />
                  ))}
                </div>

                {!!stash && stash?.length === 0 ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null}
              </div>
            )}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
export async function getServerSideProps({ params }) {
  try {
    const { userId } = params;
    let stash = await UserService.getStashByUserId(userId);
    const firstItem = stash?.content[0];
    if (firstItem) {
      const searchParams = new URLSearchParams();
      searchParams.set("limit", 20);
      searchParams.set("offset", 0);
      searchParams.set("include_orders", "false");
      searchParams.set("order_direction", "desc");
      searchParams.set("asset_contract_address", firstItem[0]?.erc721Contract);

      stash?.content.forEach((item) => {
        searchParams.append("token_ids", item[0].tokenId);
      });

      const openSeaAssetsRes = await OpenSeaService.getAssets(
        searchParams.toString()
      );

      stash = stash.content.map((item) => {
        return {
          ...item[0],
          openSeaAssets: openSeaAssetsRes.assets.filter(
            (openSeaAssetItem) => item[0].tokenId === openSeaAssetItem.token_id
          )[0],
        };
      });
    } else {
      stash = [];
    }
    return {
      props: { stashInfo: stash }, // will be passed to the page component as props
    };
  } catch (e) {
    return {
      props: { notFound: true },
    };
  }
}
