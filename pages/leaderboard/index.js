import { useEffect } from "react";
import { DefaultLayout } from "../../layouts";
import { LeaderBoardItem, Loading } from "../../components";
import { useDispatch, useSelector } from "../../hooks/useRematch";

export default function Leaderboard({ seo }) {
  const { rankList } = useSelector((state) => ({
    rankList: state?.appStore.rankList,
  }));

  const { getRankList } = useDispatch((state) => ({
    getRankList: state?.appStore.getRankList,
  }));
  useEffect(() => {
    getRankList({ pageSize: 20 });
  }, [getRankList]);

  return (
    <DefaultLayout seo={seo}>
      <div
        data-w-id="f0386187-20c7-daf5-0cad-4859c0a2b99f"
        className="overflow"
      >
        <div className="section">
          <div className="container is--left is--herol-leaderboard">
            <div className="div-leader-content">
              <img
                src="images/RS-RecordSVG.svg"
                loading="lazy"
                data-w-id="9c079e85-b034-dd18-f843-c2fae943b6b9"
                alt="A Rolling Stone Australia record"
                className="record-404 rs-leaderboard-1"
              />
              <div className="rs-leader-heading">
                <h1 className="heading-14">Leaderboard</h1>
                <p className="paragraph-11">
                  The more collectors points you have the more likely you will
                  win access to celebrity filled events and exclusive rewards,
                  your collectors rank represents your probability of reward
                  here, so there is something for everyone not just the 1%.
                </p>
              </div>
            </div>

            {rankList?.length > 0 ? (
              <div className="leaderboard">
                {rankList?.map((item) => (
                  <LeaderBoardItem
                    key={item?.userId}
                    imageURL={item?.avatar}
                    username={item?.username}
                    name={item?.name}
                    instagramURL={item?.instagram}
                    twitterURL={item?.twitter}
                    score={item?.score}
                    rank={item?.rank}
                    stashURL={`/${item?.userId}`}
                  />
                ))}
              </div>
            ) : (
              <Loading />
            )}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
Leaderboard.getInitialProps = async () => {
  return {
    seo: {
      title: "Leaderboard | Rolling Stone NFT",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
