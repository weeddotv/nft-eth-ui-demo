import Link from "next/link";
import { DefaultLayout } from "../layouts";
import { useDispatch, useSelector } from "../hooks/useRematch";
import { useEffect, useState } from "react";
import { AnchorLink, LeaderBoardItem } from "../components";
import Countdown from "react-countdown";
import { Form, Field } from "react-final-form";

export default function Home({ seo }) {
  const [subscribed, setSubscribed] = useState(false);
  const [subscriptionError, setSubscriptionError] = useState(false);
  const [buySubmiting, setBuySubmiting] = useState(false);

  const { rankList, currentSale, subscriptionLoading, auth } = useSelector(
    (state) => ({
      rankList: state?.appStore.rankList,
      currentSale: state?.userStore.currentSale,
      packsSale: state?.userStore.packsSale,
      auth: state?.userStore.auth,
      subscriptionLoading: state?.loading.effects.userStore.subscription,
    })
  );

  const { getRankList, getCurrentSale, buyNFT, subscription } = useDispatch(
    (state) => ({
      getRankList: state?.appStore.getRankList,
      getCurrentSale: state?.userStore.getCurrentSale,
      buyNFT: state?.userStore.buyNFT,
      subscription: state?.userStore.subscription,
    })
  );

  useEffect(() => {
    getRankList({ pageSize: 3 });
  }, [getRankList]);

  useEffect(() => {
    getCurrentSale();
  }, [getCurrentSale]);

  useEffect(() => {
    const buyAfterLogin = async () => {
      let item = sessionStorage.getItem("isBuying");
      if (auth?._id && item) {
        item = JSON.parse(item);
        sessionStorage.removeItem("isBuying");
        setBuySubmiting(true);
        await buyNFT({ productId: item?._id, userId: auth?._id });
        setBuySubmiting(false);
      }
    };

    buyAfterLogin();
  }, [auth, buyNFT]);

  const onBuy = async (e, item) => {
    e.preventDefault();
    if (auth) {
      setBuySubmiting(true);
      await buyNFT({ productId: item?._id, userId: auth?._id });
      setBuySubmiting(false);
    } else {
      sessionStorage.setItem("isBuying", JSON.stringify(item));
      window.location.assign("/api/auth/login");
    }
  };
  const onSubscription = async (values) => {
    try {
      await subscription(values);
      setSubscribed(true);
    } catch (e) {
      setSubscriptionError(true);
    }
  };

  return (
    <DefaultLayout seo={seo} transition="true">
      <div
        data-w-id="5ef38bc7-b298-2ffe-1899-036005436e3d"
        className="overflow"
      >
        <div
          data-w-id="89968915-5d82-98c6-2fd5-78180961c611"
          className="rs-content rs-home-animation-1"
        >
          <div className="section">
            <div className="container">
              <div className="new-hero-divk">
                <p className="sub-exp-heading">WELCOME TO DA CLUB</p>
                <p className="sub-exp-graph">by Rolling Stone Australia</p>
                <div className="kt-heading-div">
                  <div className="heading-kt-rel">
                    <div
                      data-w-id="e03d9ccd-4ab3-18bb-32b2-f073c9a7a425"
                      className="koala-img-insert  rs-home-animation-2"
                    >
                      <img
                        src="/images/K8L.png"
                        loading="lazy"
                        data-w-id="c573480e-8058-bff8-ecd1-9dc1199c8186"
                        sizes="(max-width: 479px) 48vw, (max-width: 767px) 18vw, (max-width: 991px) 100.52023315429688px, 7vw"
                        srcSet="/images/K8L-p-500.png 500w, /images/K8L.png 650w"
                        alt=""
                        className="k-circle-1"
                      />
                      <img
                        src="/images/K5.png"
                        loading="lazy"
                        data-w-id="bd8f6798-a7e9-1559-402e-a178511575ed"
                        sizes="100vw"
                        srcSet="/images/K5-p-500.png 500w, /images/K5.png 702w"
                        alt=""
                        className="k-circle-2"
                      />
                      <img
                        src="/images/K3.png"
                        loading="lazy"
                        data-w-id="d3a9492f-8ab5-4ab4-ff13-7f1aedbda721"
                        sizes="100vw"
                        srcSet="/images/K3-p-500.png 500w, /images/K3.png 568w"
                        alt=""
                        className="k-circle-3"
                      />
                      <img
                        src="/images/K6.png"
                        loading="lazy"
                        data-w-id="35033c42-96c6-8636-ca33-b69c36888a64"
                        sizes="100vw"
                        srcSet="
                        /images/K6-p-500.png  500w,
                        /images/K6-p-800.png  800w,
                        /images/K6.png       1050w
                      "
                        alt=""
                        className="k-circle-4"
                      />
                      <img
                        src="/images/K4.png"
                        loading="lazy"
                        data-w-id="95ae6b48-9e3e-d1ab-7030-39f749e57ee4"
                        sizes="100vw"
                        srcSet="
                        /images/K4-p-500.png   500w,
                        /images/K4-p-800.png   800w,
                        /images/K4-p-1080.png 1080w,
                        /images/K4.png        1118w
                      "
                        alt=""
                        className="k-circle-5"
                      />
                    </div>
                    <div className="div-koala-heading">
                      <h1 className="k-h2-hero is-mobile-header">
                        <span className="k-header-left">Membership </span>
                        to the most exclusive club for music fans on the
                        internet
                      </h1>
                      <h1 className="k-h2-hero">
                        <span className="k-header-left">Membership </span>
                        to the most exclusive club for music fans on the
                        internet
                      </h1>
                    </div>
                  </div>
                  <p className="kpara is-centre">
                    Membership is exclusive to parents who have adopted a Koala.
                    Parents will get access to parties, free merch from artists,
                    tickets and more.
                  </p>
                  <a
                    href="#live-now"
                    data-w-id="f5ed4abb-4e43-7de1-a282-3119fbb21515"
                    className="kbutton w-inline-block"
                  >
                    <div className="kbutton-white-text">ADOPT A KOALA</div>
                    <div className="div-block-33">
                      <img
                        src="/images/k-w-1JPG.svg"
                        loading="lazy"
                        alt=""
                        className="kbutton-icon"
                      />
                      <img
                        src="/images/k-w-3JPG.svg"
                        loading="lazy"
                        alt=""
                        className="kbutton-icon k-smile"
                      />
                    </div>
                  </a>
                  <p className="sale-copy">
                    <span className="k-timer">
                      {Date.now() - new Date(currentSale?.startTime).valueOf() <
                        0 && (
                        <>
                          NEXT SALE:{" "}
                          <Countdown
                            date={new Date(currentSale?.startTime).valueOf()}
                          />
                        </>
                      )}
                    </span>
                  </p>
                </div>
              </div>
              <div className="tree-div">
                <img
                  src="/images/Koala_Club_Hero_Final.jpg"
                  loading="lazy"
                  sizes="(max-width: 479px) 92vw, (max-width: 767px) 91vw, (max-width: 991px) 92vw, 89vw"
                  srcSet="
                  /images/Koala_Club_Hero_Final.jpg  500w,
                  /images/Koala_Club_Hero_Final.jpg  800w,
                  /images/Koala_Club_Hero_Final.jpg 1080w,
                  /images/Koala_Club_Hero_Final.jpg 1920w
                "
                  alt=""
                  className="image-29"
                />
                <div
                  data-w-id="04a38dd6-bd21-45d9-11b0-7f041767ab78"
                  className="k-interest-hold"
                >
                  <div className="div-pop b5">
                    <p className="white-heading-pop">
                      <strong>
                        Only 20k Koalas will be created over 10 years
                      </strong>
                    </p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      Rolling Stone will issue them over time with releases of
                      the magazine, or special features with a celebrity.
                    </p>
                  </div>
                  <div
                    data-w-id="de9f4bdb-6ab7-ca9a-2bf5-4cd5b47e681b"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
                <div className="k-interest-hold move1">
                  <div className="div-pop k-interest-adjust-3">
                    <p className="white-heading-pop">
                      Own more Koala&#x27;s to top the leaderboard
                    </p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      Although everyone has a chance, the more Koala&#x27;s you
                      own the more likely you&#x27;ll get access to special
                      events, rewards and more.
                    </p>
                  </div>
                  <div
                    data-w-id="72d08e27-c103-999b-6d34-5062c0acbdf3"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
                <div className="k-interest-hold move2">
                  <div className="div-pop k-interest-adjust-7">
                    <p className="white-heading-pop">Bear drops!</p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      Hold a Koala club and you&#x27;ll get dropped rewards,
                      tickets to celeb events and other dope stuff from our
                      roadmap.
                    </p>
                  </div>
                  <div
                    data-w-id="6cae42cb-d786-db58-c71d-8fd8b4021d6b"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
                <div className="k-interest-hold move3">
                  <div className="div-pop b7">
                    <p className="white-heading-pop">
                      Mix with Australian music celebrates
                    </p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      Some koala&#x27;s are generic, some are released in
                      partnership with real Aussie musicians. You&#x27;ve never
                      been more connected to Australian music royalty.
                    </p>
                  </div>
                  <div
                    data-w-id="61d701da-bafc-d36e-105f-42537f98c084"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
                <div className="k-interest-hold move4">
                  <div className="div-pop k-interest-adjust-4">
                    <p className="white-heading-pop">
                      Cigar lounge with Rolling Stone Journalists
                    </p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      Koala Club holders have special VIP access to a Discord
                      Channel where you&#x27;ll be able to talk to and influence
                      Rolling Stone Australia&#x27;s journalists.
                    </p>
                  </div>
                  <div
                    data-w-id="91044d78-caa1-0c2f-152b-9155370cd1c7"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
                <div className="k-interest-hold move5">
                  <div className="div-pop k-interest-adjust-2">
                    <p className="white-heading-pop">
                      Supporting Aussie music, and Aussie animals
                    </p>
                    <div className="k-red-pop-line"></div>
                    <p className="k-para-pop-white">
                      We&#x27;ll be making donations to help Australian native
                      wildlife. As a club we believe that&#x27;s important.
                    </p>
                  </div>
                  <div
                    data-w-id="cd4e1c13-54db-1d8b-b493-0c425f23f2eb"
                    className="k-red-circle-in"
                  >
                    <img
                      src="/images/k-w-3JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is--smile"
                    />
                    <img
                      src="/images/k-w-1JPG.svg"
                      loading="lazy"
                      alt=""
                      className="image-27 is-round"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="section">
            <div className="container">
              <div className="khero-text-div">
                <div className="kpara-div">
                  <div className="k-space">
                    <h2 className="k-h2">
                      <strong>
                        Adopt a Koala that matches your music taste
                      </strong>
                    </h2>
                  </div>
                  <p className="kpara is-text-centre">
                    <em>Rolling Stone </em>will put Koalas up for adoption
                    sporadically and in limited fashion over the next 10 years.
                    A maximum of 20k Koalas will be minted, making each Koala
                    extremely rare.{" "}
                    <Link href="/cmsfaqs/faq-content">
                      <a className="is-underlined">All the details</a>
                    </Link>
                  </p>
                  <div className="kbutton-grid">
                    <a
                      href="#live-now"
                      data-w-id="d17a8ca0-ba92-a212-4983-edeb67f04620"
                      className="kbutton w-inline-block"
                    >
                      <div className="kbutton-white-text">ADOPT A KOALA</div>
                      <div className="div-block-33">
                        <img
                          src="https://assets.website-files.com/611c8104e15cb659481fb48d/626cb235a5f90a098702da69_k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="kbutton-icon"
                        />
                        <img
                          src="https://assets.website-files.com/611c8104e15cb659481fb48d/626cb235e6db3a238ac093b1_k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="kbutton-icon k-smile"
                        />
                      </div>
                    </a>
                  </div>
                </div>
                <div className="knft-div">
                  <div className="kdiv-block-contain">
                    <div className="knft-single-div is--left">
                      <img
                        src="/images/k-purpleJPG.jpg"
                        loading="lazy"
                        alt=""
                        className="koala-main"
                      />
                      <img
                        src="/images/square-white-redSVG.svg"
                        loading="lazy"
                        alt=""
                        className="image-15"
                      />
                      <div className="kdesciption-text">
                        Daniel Johns Koala <strong>#120</strong>
                      </div>
                    </div>
                    <div className="knft-single-div">
                      <img
                        src="/images/koala-nftJPEG.jpg"
                        loading="lazy"
                        alt=""
                        className="koala-main"
                      />
                      <img
                        src="/images/red-squareSVG.svg"
                        loading="lazy"
                        alt=""
                        className="image-15"
                      />
                      <div className="kdesciption-text">
                        Daniel Johns Koala <strong>#11</strong>
                      </div>
                    </div>
                  </div>
                  <div className="kdiv-block-contain is--bottom">
                    <div className="knft-single-div is--left">
                      <img
                        src="/images/k-orangeJPG.jpg"
                        loading="lazy"
                        alt=""
                        className="koala-main"
                      />
                      <img
                        src="/images/red-squareSVG.svg"
                        loading="lazy"
                        alt=""
                        className="image-15"
                      />
                      <div className="kdesciption-text">
                        Daniel Johns Koala <strong>#30</strong>
                      </div>
                    </div>
                    <div className="knft-single-div">
                      <img
                        src="/images/k-greenJPG.jpg"
                        loading="lazy"
                        alt=""
                        className="koala-main"
                      />
                      <img
                        src="/images/red-squareSVG.svg"
                        loading="lazy"
                        alt=""
                        className="image-15"
                      />
                      <div className="kdesciption-text">
                        Daniel Johns Koala <strong>#204</strong>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kbottom-notes">
                <div className="kpoint-div-contain">
                  <div className="kpoint-div">
                    <img
                      src="/images/Open-seaSVG.svg"
                      loading="lazy"
                      alt=""
                      className="image-17"
                    />
                    <div className="text-block-6">Secondary Markets</div>
                    <p className="kpara is-text-centre">
                      Missed out on a sale? Check out Koala availability on
                      secondary markets like
                      <a
                        href="https://opensea.io"
                        target="_blank"
                        rel="noreferrer"
                        className="link-7"
                      >
                        {" "}
                        Open Sea
                      </a>
                    </p>
                  </div>
                  <div className="kpoint-div is--right">
                    <img
                      src="/images/The-Lions-Share.png"
                      loading="lazy"
                      alt=""
                      className="image-17"
                    />
                    <div className="text-block-6">Support Aussie Animals</div>
                    <p className="kpara is-text-centre">
                      A few Koala&#x27;s from every release will go to The
                      Lion&#x27;s Share, supporting Aussie wildlife conservation
                      and animal welfare.
                    </p>
                  </div>
                </div>
                <div className="text-block-7">
                  Note: some Koala’s from each release will be withheld from
                  sale. These will be used for givaways, rewards, and the
                  creators’ memberships.
                </div>
              </div>
              <div className="kred-line"></div>
            </div>
          </div>
          <div id="live-now" className="live-now">
            <div id="is-LIVE-SINGLE" className="section is--live-single">
              <div className="container">
                <div className="ksale-cover">
                  <div className="klive-div is-adjusted-left">
                    <div className="krecord-div">
                      <img
                        src="/images/RS-RecordSVG.svg"
                        loading="lazy"
                        alt="A Rolling Stone Australia record"
                        className="krecord"
                      />
                    </div>
                    <h2 className="k-h2">
                      {currentSale?.collectionName}:{" "}
                      <span className="text-span-3">
                        {Date.now() -
                          new Date(currentSale?.startTime).valueOf() <
                        0 ? (
                          <Countdown
                            date={new Date(currentSale?.startTime).valueOf()}
                          />
                        ) : (
                          "Live now"
                        )}
                      </span>
                    </h2>
                  </div>
                  <div className="klive-sales-contents">
                    {currentSale?.products?.map((item) => (
                      <div className="ksale-live-box" key={item._id}>
                        <div className="div-block-36">
                          <div className="knft-single-div is--left">
                            <img
                              src={item.image}
                              loading="lazy"
                              alt="koala"
                              className="koala-main kz-1"
                            />

                            <img
                              src="/images/red-squareSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-15"
                            />
                          </div>
                          <div className="ksale-div-heading">
                            <div className="ksale-info">
                              <div className="ksale-item-heading">
                                {item.name}
                              </div>
                              <div className="ksale-price">${item.price}</div>
                            </div>
                            <div className="kred-line-sub"></div>
                            <div>
                              <div className="ksale-item-heading is--red">
                                {item.available} of {item.issuedSupply} left
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="div-block-34">
                          <div className="kbullet-list-record">
                            <div className="kbullet-div">
                              <div className="kicon-image-div">
                                <img
                                  src="/images/RSBulletSVG.svg"
                                  loading="lazy"
                                  alt=""
                                  className="image-14"
                                />
                              </div>
                              <p className="kpara">
                                Mint 1 random, one of a kind Koala
                              </p>
                            </div>
                            <div className="kbullet-div">
                              <div className="kicon-image-div">
                                <img
                                  src="/images/RSBulletSVG.svg"
                                  loading="lazy"
                                  alt="koala"
                                  className="image-14"
                                />
                              </div>
                              <p className="kpara">
                                Become a Rolling Stone Koala Club member
                              </p>
                            </div>
                            <div className="kbullet-div">
                              <div className="kicon-image-div">
                                <img
                                  src="/images/RSBulletSVG.svg"
                                  loading="lazy"
                                  alt=""
                                  className="image-14"
                                />
                              </div>
                              <p className="kpara">
                                Get access to special events and rewards
                              </p>
                            </div>
                            <div className="kbullet-div">
                              <div className="kicon-image-div">
                                <img
                                  src="/images/RSBulletSVG.svg"
                                  loading="lazy"
                                  alt=""
                                  className="image-14"
                                />
                              </div>
                              <p className="kpara">
                                Each Koala is 1 Leaderboard point
                              </p>
                            </div>
                          </div>
                          <div className="kbutton-grid-space">
                            <button
                              data-w-id="9a3d3c2b-2aae-86b4-4249-7f12607018ab"
                              onClick={(e) => onBuy(e, item)}
                              className="kbutton w-inline-block"
                              disabled={buySubmiting || item.available === 0}
                            >
                              <div className="kbutton-white-text">
                                {!buySubmiting
                                  ? "ADOPT A KOALA"
                                  : "Please wait..."}
                              </div>
                              <div className="div-block-33">
                                <img
                                  src="/images/k-w-1JPG.svg"
                                  loading="lazy"
                                  alt=""
                                  className="kbutton-icon"
                                />
                                <img
                                  src="/images/k-w-3JPG.svg"
                                  loading="lazy"
                                  alt=""
                                  className="kbutton-icon k-smile"
                                />
                              </div>
                            </button>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                  <div className="kred-line"></div>
                </div>
              </div>
            </div>
            <div id="is-SOLD" className="section is--sold">
              <div className="container">
                <div className="ksale-cover">
                  <div className="ksold-contents">
                    <div className="ksale-left">
                      <div className="ksale-div-heading is--ksold">
                        <div className="ksale-info">
                          <div className="k-collection-name">
                            Daniel John&#x27;s Collection
                          </div>
                        </div>
                        <div className="kred-line-sub"></div>
                        <div className="div-block-27">
                          <div className="ksale-item-heading is--red-space">
                            SOLD OUT
                          </div>
                          <div className="k-sold-date">on 30th Apr 2022</div>
                        </div>
                      </div>
                      <div className="div-block-29">
                        <div className="knft-single-div is--multiple">
                          <img
                            src="/images/k-purpleJPG.jpg"
                            loading="lazy"
                            alt=""
                            className="koala-main kz-1"
                          />
                          <img
                            src="/images/red-squareSVG.svg"
                            loading="lazy"
                            alt=""
                            className="image-15"
                          />
                        </div>
                        <div className="knft-single-div is--multiple">
                          <img
                            src="/images/k-orangeJPG.jpg"
                            loading="lazy"
                            alt=""
                            className="koala-main kz-1"
                          />
                          <img
                            src="/images/red-squareSVG.svg"
                            loading="lazy"
                            alt=""
                            className="image-15"
                          />
                        </div>
                      </div>
                      <div className="k-sold-points-div">
                        <div className="k-sold-heading">
                          <div className="ksale-info">
                            <div className="ksale-item-heading">
                              RSKC Members
                            </div>
                          </div>
                        </div>
                        <div className="kbullet-list-record">
                          <div className="kbullet-div">
                            <div className="kicon-image-div">
                              <img
                                src="/images/RSBulletSVG.svg"
                                loading="lazy"
                                alt=""
                                className="image-14"
                              />
                            </div>
                            <p className="kpara">
                              Join the members only RSKC{" "}
                              <a href="#" className="link-6">
                                Discord Group
                              </a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="ksale-right-option is-alternative">
                      <div className="ksale-div-heading is-callout">
                        <div className="ksale-info">
                          <div className="ksale-item-heading">
                            Next Koala Collection Alert:
                          </div>
                        </div>
                        <div className="kred-signup">
                          <p className="paragraph-13 is--ch-fixed">
                            Don&#x27;t miss a new celebrity Koala collection
                            roughly every 3 months. Be the first in line:
                          </p>
                          <div className="footer-form w-form">
                            {!subscribed ? (
                              <Form
                                onSubmit={onSubscription}
                                render={({ handleSubmit }) => (
                                  <form
                                    id="email-form"
                                    name="email-form"
                                    data-name="Email Form"
                                    className="form"
                                    onSubmit={handleSubmit}
                                  >
                                    <Field name="email">
                                      {({ input }) => (
                                        <>
                                          <input
                                            type="email"
                                            className="form-feild w-input"
                                            maxLength="256"
                                            name="name-2"
                                            data-name="Name 2"
                                            placeholder="Your email"
                                            id="name-2"
                                            {...input}
                                          />
                                        </>
                                      )}
                                    </Field>

                                    <input
                                      type="submit"
                                      value={
                                        !subscriptionLoading
                                          ? "GET ALERTED"
                                          : "Please wait..."
                                      }
                                      className="submit-button w-button"
                                    />
                                  </form>
                                )}
                              />
                            ) : (
                              <div
                                className="success-message w-form-done"
                                style={{ display: "block" }}
                              >
                                <div className="error-text">
                                  Thank you! You&#x27;ll hear from us soon.
                                </div>
                              </div>
                            )}

                            {subscriptionError && (
                              <div className="error-message-2 w-form-fail">
                                <div className="error-text">
                                  Hmmm, something went wrong. Please try again!
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="k-secondary">
                        <img
                          src="/images/Open-seaSVG.svg"
                          loading="lazy"
                          alt=""
                          className="open-sea-img"
                        />
                        <div className="div-block-28">
                          <div className="k-sold-heading">
                            <div className="ksale-info">
                              <div className="ksale-item-heading">
                                Can&#x27;t wait? Try the market
                              </div>
                            </div>
                          </div>
                          <p className="kpara is--chlimit">
                            You may be able to find a Koala on a secondary
                            markets such as Open Sea. Once you buy remember to
                            come back and{" "}
                            <AnchorLink
                              href="/cmsfaqs/faq-content"
                              className="link-6"
                            >
                              register
                            </AnchorLink>{" "}
                            your Koala to get all the club benefits
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="kred-line"></div>
                </div>
              </div>
            </div>
          </div>
          <div className="section">
            <div className="container">
              <div className="ksale-cover">
                <div className="kheader-sub">
                  <div className="kroadmap-heading">
                    <h2 className="k-h2">
                      The Koala Club&#x27;s 1st Member Roadmap
                    </h2>
                    <div className="paradiv-space">
                      <p className="kpara is--longer">
                        Below is the first round of what we&apos;re planning for
                        members. Only more will follow. Kayne West said it best:
                        &quot;Cause my life is dope and I do{" "}
                        <strong>
                          <em>dope</em>
                        </strong>{" "}
                        shit.&quot;
                      </p>
                    </div>
                  </div>
                  <div className="ktestimonial">
                    <div className="ktest-topdiv">
                      <div className="luke-div">
                        <img
                          src="/images/Luke-headshotPNG.png"
                          loading="lazy"
                          alt=""
                          className="image-19"
                        />
                        <div className="text-block-8">
                          Luke Girgis - CEO - Rolling Stone
                        </div>
                      </div>
                    </div>
                    <div className="ktest-bottomdiv">
                      <p className="paragraph-13">
                        It ain’t just a pretty Koala face you’re getting.
                        Rolling Stone Australia is building the best online
                        community for music fans.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="klive-roadmap-content">
                  <div className="div-block-38">
                    <img
                      src="/images/Koala_Club_Roadmap_Finalv20.jpg"
                      loading="lazy"
                      sizes="(max-width: 479px) 92vw, (max-width: 767px) 91vw, (max-width: 991px) 92vw, 89vw"
                      srcSet="
                      /images/Koala_Club_Roadmap_Finalv20.jpg  500w,
                      /images/Koala_Club_Roadmap_Finalv20.jpg  800w,
                      /images/Koala_Club_Roadmap_Finalv20.jpg 1080w,
                      /images/Koala_Club_Roadmap_Finalv20.jpg 1920w
                    "
                      alt=""
                      className="image-21"
                    />
                    <div className="k-interest-hold mover2">
                      <div className="div-pop b3">
                        <p className="white-heading-pop">Stoned Koala&#x27;s</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Held back and dropped to random Koala holders
                        </p>
                      </div>
                      <div
                        data-w-id="e2887997-9501-e88e-f3d9-05c5cdf2adf3"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover1">
                      <div className="div-pop k-interest-adjust-k2">
                        <p className="white-heading-pop">Bear drops</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Koala holders will receive Artist exclusive giveaways
                          and meet-and-greets. Ticket and event giveaways.
                        </p>
                      </div>
                      <div
                        data-w-id="b6d9535d-be4d-de9b-def6-55d1ec9ea36b"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover20">
                      <div className="div-pop k-b1">
                        <p className="white-heading-pop">Club mural</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Commissioned in honour of the club. May feature a
                          random members Koala, making it &#x27;famous&#x27;.
                        </p>
                      </div>
                      <div
                        data-w-id="835cfb7c-28b8-23ee-6c1a-774a60c06891"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover3">
                      <div className="div-pop k-interest-adjust-10">
                        <p className="white-heading-pop">Vinyl club</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Let&#x27;s get back to basics and celebrate Aussie
                          music.
                        </p>
                      </div>
                      <div
                        data-w-id="725e2ae3-92d2-52cf-c443-d63ba999b10c"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold movek31">
                      <div className="div-pop k-interest-adjust-k30">
                        <p className="white-heading-pop">
                          Tree house leaderboard
                        </p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Each Koala allows the holder 1 point. While everyone
                          stands a chance, you&#x27;ll have a higher likehood
                          for some rewards the higher your position.
                        </p>
                      </div>
                      <div
                        data-w-id="9d65f925-48fd-0779-991a-701437bc1934"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover10">
                      <div className="div-pop k-interest-adjust-11">
                        <p className="white-heading-pop">Live event</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Get together with other Koala Club members for the
                          time of your life as a VIP
                        </p>
                      </div>
                      <div
                        data-w-id="d15fde7b-c6ef-12af-d064-79e4947e0ebb"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover5">
                      <div className="div-pop k-interest-adjust-k1">
                        <p className="white-heading-pop">Bear drops</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Artist exclusive giveaways and meet-and-greets. Ticket
                          and event giveaways.
                        </p>
                      </div>
                      <div
                        data-w-id="1db14ac8-7825-13af-21ae-3e703f40521b"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover9">
                      <div className="div-pop b2">
                        <p className="white-heading-pop">
                          Make my Koala famous
                        </p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          every quarter Rolling Stone will make a random Koala
                          &#x27;famous&#x27; by featuring it in the magazine,
                          events and more.
                        </p>
                      </div>
                      <div
                        data-w-id="62834c4e-3d3c-f3a2-b49c-6e9282696e2a"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                    <div className="k-interest-hold mover7">
                      <div className="div-pop k-interest-adjust-1">
                        <p className="white-heading-pop">Cigar Lounge (VIP)</p>
                        <div className="k-red-pop-line"></div>
                        <p className="k-para-pop-white">
                          Speak directly with Rolling Stone journalists in a VIP
                          setting. Influence the influencers.
                        </p>
                      </div>
                      <div
                        data-w-id="d2ea8209-b825-85c8-99a2-31dd1cd8b44b"
                        className="k-red-circle-in"
                      >
                        <img
                          src="/images/k-w-3JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is--smile"
                        />
                        <img
                          src="/images/k-w-1JPG.svg"
                          loading="lazy"
                          alt=""
                          className="image-27 is-round"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="kroadmap-unlock-bullets">
                    <div className="div-block-20">
                      <div className="text-block-8">Map Key</div>
                    </div>
                    <div className="div-block-21">
                      <div className="kbullet-list-road">
                        <div className="kbullet-div is--road">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Tree house leaderboard for reward prioritisation
                          </p>
                        </div>
                        <div className="kbullet-div is--road">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Stoned&#x27; Koala’s held back and ‘dropped’ to
                            random Koala holders
                          </p>
                        </div>
                        <div className="kbullet-div is--road">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Bear drops: Artist exclusive giveaways and
                            meet-and-greets
                          </p>
                        </div>
                        <div className="kbullet-div is--road">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Bear drops: Ticket and event giveaways
                          </p>
                        </div>
                      </div>
                      <div className="kbullet-list-road">
                        <div className="kbullet-div kroadmap-key-heading">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Rolling Stone Cigar Lounge (speak directly with
                            Rolling Stone journalists in a VIP setting)
                          </p>
                        </div>
                        <div className="kbullet-div kroadmap-key-heading">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">Live event for members</p>
                        </div>
                        <div className="kbullet-div kroadmap-key-heading">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">Vinyl club</p>
                        </div>
                        <div className="kbullet-div kroadmap-key-heading">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">Club mural</p>
                        </div>
                        <div className="kbullet-div kroadmap-key-heading">
                          <div className="kicon-image-div">
                            <img
                              src="/images/RSBulletSVG.svg"
                              loading="lazy"
                              alt=""
                              className="image-14"
                            />
                          </div>
                          <p className="kpara">
                            Make my Koala famous: every quarter Rolling Stone
                            will make a random Koala &#x27;famous&#x27; by
                            featuring it in the magazine, events and more. You
                            own your Koala - but boy isn&#x27;t it fun to have
                            famous connections to promote it!
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="kred-line"></div>
              </div>
            </div>
          </div>
          <div className="section">
            <div className="container">
              <div className="kheader-sub is--leaderboard">
                <div className="kleft-section">
                  <h2 className="k-h2">Club Leaderboard</h2>
                  <div className="paradiv-space is--space-below">
                    <p className="kpara is--longer is--centre">
                      Every Koala is 1 point. The more Koala’s you have, the
                      higher in the leaderboard you are. That means a higher
                      chance at exclusive event tickets, rewards and more.
                    </p>
                  </div>
                  <div className="kbutton-grid-space is--centre">
                    <Link href="/leaderboard">
                      <a
                        data-w-id="30e56afa-0bc4-128a-d674-ec7a4c6d0178"
                        className="kbutton w-inline-block"
                      >
                        <div className="kbutton-white-text">LEADERBOARD</div>
                        <div className="div-block-33">
                          <img
                            src="/images/k-w-1JPG.svg"
                            loading="lazy"
                            alt=""
                            className="kbutton-icon"
                          />
                          <img
                            src="/images/k-w-3JPG.svg"
                            loading="lazy"
                            alt=""
                            className="kbutton-icon k-smile"
                          />
                        </div>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="leaderboard">
                {rankList?.map((item) => (
                  <LeaderBoardItem
                    key={item?.userId}
                    imageURL={item?.avatar}
                    username={item?.username}
                    name={item?.name}
                    score={item?.score}
                    rank={item?.rank}
                    stashURL={`/${item?.userId}`}
                  />
                ))}
              </div>
            </div>
          </div>
          <div className="section">
            <div className="container">
              <div className="div-block-23">
                <h2 className="k-h2">The first rules of RSKC</h2>
              </div>
              <div className="krules-div-container">
                <div className="k-rules-item">
                  <div className="text-block-6">Only 20k</div>
                  <p className="kpara is--longer">
                    Koalas over 10 years of releases
                  </p>
                </div>
                <div className="k-rules-item">
                  <div className="text-block-6">Access</div>
                  <p className="kpara is--longer">
                    to Rolling Stone journalists
                  </p>
                </div>
                <div className="k-rules-item">
                  <div className="text-block-6">Celebrity</div>
                  <p className="kpara is--longer">
                    event access and more at exclusive events
                  </p>
                </div>
                <div className="k-rules-item">
                  <div className="text-block-6">Bear drops</div>
                  <p className="kpara is--longer">
                    Rewards, exclusive merch and more
                  </p>
                </div>
                <div className="k-rules-item">
                  <div className="text-block-6">Roadmap</div>
                  <p className="kpara is--longer">
                    Lots of activations to come for members
                  </p>
                </div>
                <div className="k-rules-item">
                  <div className="text-block-6">More</div>
                  <p className="kpara is--longer">
                    Check out all the rules{" "}
                    <Link href="/cmsfaqs/faq-content">
                      <a className="link-6">right here.</a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}

Home.getInitialProps = async () => {
  return {
    seo: {
      title: "Koala Home",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
