import { DefaultLayout } from "../../layouts";

export default function ErrorPage() {
  return (
    <DefaultLayout>
      <div className="utility-page-wrap">
        <img
          src="images/RS-RecordSVG.svg"
          loading="lazy"
          alt="A Rolling Stone Australia record"
          className="record-404"
        />
        <h1 className="heading-15">Page not found</h1>
        <p className="paragraph-12">
          404 Error. Get <a href="#">back to collecting</a>
        </p>
      </div>
    </DefaultLayout>
  );
}
