import "antd/dist/antd.css";
import "../public/css/normalize.css";
import "../public/css/components.css";
import "../public/css/rollingstone.css";
import "../styles/common.scss";

import App from "next/app";
import { Provider } from "react-redux";
import { UserProvider } from "@auth0/nextjs-auth0";
import { useScript } from "../hooks";
import { useEffect } from "react";
import { useRouter } from "next/router";
import store from "../stores";
import Script from "next/script";

function MyApp({ Component, pageProps }) {
  const Router = useRouter();

  const jqueryStatus = useScript(
    "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
  );

  useEffect(() => {
    if (Router && window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [Router]);

  return (
    <Provider store={store}>
      <UserProvider>
        <Component {...pageProps} />
      </UserProvider>
      {jqueryStatus === "ready" && (
        <>
          <Script src="/js/rollingstone.js" />
          <Script src="/js/custom.js" />
        </>
      )}
    </Provider>
  );
}

MyApp.getServerSideProps = async () => {
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps };
};

export default MyApp;
