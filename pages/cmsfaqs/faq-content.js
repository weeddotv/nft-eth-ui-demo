import { DefaultLayout } from "../../layouts";

export default function FAQ({ seo }) {
  return (
    <DefaultLayout seo={seo}>
      <div
        data-w-id="2a55fe77-ee0e-27e5-a04c-36d1561189e0"
        className="overflow"
      >
        <div className="section">
          <div className="container is--left is--hero">
            <h1>Project overview</h1>
            <div className="rich-text-block-3 is-rules w-richtext"></div>
            <h1>FAQ</h1>
            <div className="rich-text-block-3 w-richtext"></div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}

FAQ.getInitialProps = async () => {
  return {
    seo: {
      title: "FAQ | Rolling Stone NFT",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
