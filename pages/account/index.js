import { useState } from "react";
import { Field, Form } from "react-final-form";
import { useSelector, useDispatch } from "../../hooks";
import { DefaultLayout } from "../../layouts";
import { shortAddress } from "../../utils/web3Utilities";

export default function Account({ seo }) {
  const [editData, setEditData] = useState(null);
  const { auth, connectMetamaskLoading } = useSelector((state) => ({
    auth: state.userStore.auth,
    updateMeLoading: state.loading.effects.userStore.updateMe,
    connectMetamaskLoading: state.loading.effects.userStore.connectMetamask,
  }));
  const { updateMe, connectMetamask } = useDispatch((state) => ({
    updateMe: state.userStore.updateMe,
    connectMetamask: state.userStore.connectMetamask,
  }));

  const updatePrivate = (currentPrivate) => {
    updateMe({ private: !currentPrivate });
  };

  const onConnectMetamask = () => {
    connectMetamask();
  };

  const onEdit = (e, type) => {
    e.preventDefault();
    switch (type) {
      case "address": {
        setEditData({
          addressEditing: true,
          usernameEditing: false,
        });
        break;
      }
      case "username": {
        setEditData({
          addressEditing: false,
          usernameEditing: true,
        });
        break;
      }
      default:
        break;
    }
  };

  const onUpdate = async (values) => {
    await updateMe(values);
    setEditData({
      addressEditing: false,
      usernameEditing: false,
    });
  };

  // if(!auth?.score) return null;
  return (
    <DefaultLayout showFooter={false} seo={seo}>
      <div
        data-w-id="37488e27-ec65-d120-bdcc-813c5eff73d9"
        className="overflow rs-s-account"
      >
        <div className="section">
          <div className="container is--account">
            <div className="centre-contents is-narrower">
              <div className="picture-div is---space">
                <img
                  src={auth?.avatar || "/images/defaultAvatar.svg"}
                  loading="lazy"
                  alt="Avatar for user account page"
                  className="image-8 rol-avatar"
                />
              </div>
              <div className="personal-div">
                <p className="paragraph-4">@{auth?.username || auth?.name}</p>
              </div>
            </div>

            <div className="centre-contents is--aligned">
              {!auth?.ethereumAddress && (
                <div className="meta-div">
                  <button
                    className="meta-button"
                    onClick={onConnectMetamask}
                    disabled={connectMetamaskLoading}
                  >
                    <img
                      src="images/MetaMask_Fox.svg"
                      loading="lazy"
                      alt=""
                      className="image-30"
                    />
                    <p className="paragraph-17">
                      {!connectMetamaskLoading ? "Connect" : "Please wait..."}{" "}
                    </p>
                  </button>
                </div>
              )}
              <div className="transaction-row is--accounts is-vert">
                <div className="div-block-39">
                  <div className="transaction-text is-vert">Email:</div>
                  <div className="transaction-text is-vert"> {auth?.email}</div>
                </div>
              </div>

              <Form
                onSubmit={onUpdate}
                initialValues={{
                  username: auth?.username || auth?.name,
                  address: auth?.address,
                }}
                render={({ handleSubmit }) => (
                  <form className="rs-form" onSubmit={handleSubmit}>
                    <Field name="address">
                      {({ input }) => (
                        <div className="transaction-row is--accounts is-vert">
                          <div className="div-block-41">
                            <div className="transaction-text is-vert">
                              Address:{" "}
                            </div>
                            <div className="transaction-text is-vert">
                              {editData?.addressEditing ? (
                                <input
                                  type="text"
                                  className="rs-edit-input"
                                  maxLength="256"
                                  placeholder="address"
                                  autoFocus
                                  {...input}
                                />
                              ) : (
                                auth?.address
                              )}
                            </div>
                          </div>

                          {!editData?.addressEditing ? (
                            <button
                              type="button"
                              className="link is-narrower is--vert"
                              onClick={(e) => onEdit(e, "address")}
                            >
                              edit
                            </button>
                          ) : (
                            <button
                              type="submit"
                              className="link is-narrower is--vert"
                            >
                              save
                            </button>
                          )}
                        </div>
                      )}
                    </Field>
                    <Field name="username">
                      {({ input }) => (
                        <div className="transaction-row is--accounts is-vert">
                          <div className="div-block-41">
                            <div className="transaction-text is-vert">
                              Name:{" "}
                            </div>
                            <div className="transaction-text is-vert">
                              @
                              {editData?.usernameEditing ? (
                                <input
                                  type="text"
                                  className="rs-edit-input"
                                  maxLength="256"
                                  placeholder="user name"
                                  autoFocus
                                  {...input}
                                />
                              ) : (
                                auth?.username
                              )}
                            </div>
                          </div>
                          {!editData?.usernameEditing ? (
                            <button
                              type="button"
                              className="link is-narrower is--vert"
                              onClick={(e) => onEdit(e, "username")}
                            >
                              edit
                            </button>
                          ) : (
                            <button
                              type="submit"
                              className="link is-narrower is--vert"
                            >
                              save
                            </button>
                          )}
                        </div>
                      )}
                    </Field>
                  </form>
                )}
              />

              {auth?.ethereumAddress && (
                <div className="transaction-row is--accounts is-vert">
                  <div className="div-block-39">
                    <div className="transaction-text is-vert">Wallet:</div>
                    <div className="transaction-text is-vert">
                      {" "}
                      {shortAddress(auth?.ethereumAddress)}
                    </div>
                  </div>
                  <button
                    onClick={onConnectMetamask}
                    className="link is-narrower is--vert"
                  >
                    edit
                  </button>
                </div>
              )}

              <div className="transaction-row is--accounts is-vert is--info">
                <div className="div-block-15">
                  <div className="transaction-text is-vert">Private:</div>
                  <button
                    className="link is-narrower is--vert"
                    onClick={() => updatePrivate(auth?.private)}
                  >
                    {auth?.private ? "on" : "off"}
                  </button>
                </div>
                <p className="info">Turn on to hide from leaderboard</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
Account.getInitialProps = async () => {
  return {
    seo: {
      title: "Account | Rolling Stone NFT",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
