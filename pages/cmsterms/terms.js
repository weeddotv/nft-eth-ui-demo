import { DefaultLayout } from "../../layouts";

export default function Terms({ seo }) {
  return (
    <DefaultLayout seo={seo}>
      <div
        data-w-id="722f4fc3-1ecc-2df5-aa59-b05c0654057b"
        className="overflow"
      >
        <div className="section">
          <div className="container is--left is--hero">
            <h1>Terms</h1>
            <div className="w-dyn-list">
              <div role="list" className="w-dyn-items">
                <div role="listitem" className="w-dyn-item">
                  <div className="rich-text-block-3 w-richtext"></div>
                </div>
              </div>
              <div className="w-dyn-empty">
                <div>No items found.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}

Terms.getInitialProps = async () => {
  return {
    seo: {
      title: "Legal | Rolling Stone NFT",
      description:
        "Every month Rolling Stone will release exclusive NFT’s with every cover. For 50 years we’ve reported to you on culture. Now it&#x27;s your chance to own it.",
    },
  };
};
